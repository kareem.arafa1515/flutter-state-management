
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'counter_scope_model.dart';

class MyApp extends StatelessWidget {
  final CounterModel model = CounterModel();

  @override
  Widget build(BuildContext context) {
    return ScopedModel<CounterModel>(
        model: model,
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: MyHomePage(title: 'Flutter Demo Home Page'),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CounterView(),
            IncrementButton(),
          ],
        ),
      ),
    );
  }
}

class CounterView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Text(
        'You have pushed the button this many times:',
      ),
      ScopedModelDescendant<CounterModel>(builder: (context, child, model) {
        return Text(
          '${model.counter}',
          style: Theme.of(context).textTheme.headline4,
        );
      })
    ]));
  }
}

class IncrementButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CounterModel>(
        builder: (context, child, model) {
      return ElevatedButton(
        onPressed: model.incrementCounter,
        child: Container(
          child: Text("Increment"),
        ),
      );
    });
  }
}
